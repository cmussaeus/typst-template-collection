#import "../templates/main.typ": chapter, example, definition
#show: chapter.with(title: "The first chapter")

= Some chapter

#lorem(100)

== Some subchapter

#lorem(100)

== Another subchapter

#lorem(100)

#example[
  #lorem(50)
]

#definition[
  #lorem(40)
]

#definition(boxed: false)[
  #lorem(8)
]

= Another big chapter

#lorem(50)

#lorem(150)
